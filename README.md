# Big Bad Evil StartPlaying.Games Assets

This repository contains assets to enhance [StartPlaying.games](https://startplaying.games/) adventure listings, currently including thuhmbnails and thumbnail templates.

This collection of free assets was founded by the [Big Bad Evil Gamemaster](https://startplaying.games/gm/bbegm). If you wish to support future free art-making endeavors, you can toss me a coin [here](https://ko-fi.com/bbegm).

## Contents

- `templates/`: Props to make new thumbnails, faster.
  - `bbegm/`: Thumbnail template project files, including guide layer and vignette layer, for gimp and photoshop, by me.
- `thumbnails/`: Ready-made thumbnails for SPG. Original images have been edited to highlight interesting themes, infer action and adventure, attract the eye, and conform to best visual practices on SPG. Each `thumbnail.jpeg` is a 1600x900 image under 1 MiB in size and does not conflict with StartPlaying thumbnail UI elements. A subtle vignette has been applied around the edges of each, to guide viwers' eyes to the center of the thumbnail. Some images have been cropped, aligned, flipped, or otherwise adapted to better suit the medium. Works are organized by owner/artist. Each artist's directory contains an `archives` and a `previews` subdirectory. Archives are `.zip`s containing the original artwork, GIMP and Photoshop project files, full-resolution thumbnails, and credit/license information. Previews are downscaled versions of each `thumbnail.jpeg`. It is not recommended to use the previews in production, a higher quality version can be found in the archive of the same name, but the previews allow you to view the image independently, without downloading and unzipping the archive. The index below describes common themes explored in each artists's works. Refer to the `credit.txt` and `license.txt` files included with each work to determine your attribution obligations. You never have to credit me, but you may have to credit the original artist. I am not a lawyer. Verify your legal rights to this art for yourself.
  - `chamomilehasadventures/custin`: Fantasy, monsters (8 works)
  - `david-revoy`: Cats, comedy, fantasy, feminine protagonists, landscapes, magic, monsters, rustic, witches, sci-fi, urban (197 works)
  - `great-tomb-productions/lutvi-kurniawan/`: Magic, monsters, undeath (22 works)

## Goals

- [] Enhance colors of existing thumbnails
- [] More thumbnails in a wider variety of genres
- [] Platform icon templates (add a small Foundry VTT or D&D 5e logo to your thumbnail)
- [] Ad copy templates
- [] GM Profile banner template

## Sources

This collection of adaptable and sharable assets was compiled with fair-use media acquired from:

- [ChamomileHasIllustrations](https://chamomilehasgames.itch.io/chamomile-has-illustrations)
- [David Revoy](https://www.davidrevoy.com/)
- [Great Tomb Productions](https://greattomb.com/)
- [Pepper & Carrot Webcomic](https://www.peppercarrot.com/)
